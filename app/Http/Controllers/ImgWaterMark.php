<?php

namespace App\Http\Controllers;

use function foo\func;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImgWaterMark extends Controller
{
    public function index()
    {
        return view('img');
    }

    public function imgPost(Request $request)
    {
        $image = $request->file('image');

       $sended_image = Image::make($image);
        $sended_image->resize(150,150);
       $width = intval($sended_image->width());
       $height = intval($sended_image->height());
//       dd($width,$height);
       $start_x = intval(round($width * 0.13));
       $start_y = intval(round($height * 0.13));

       $positions = ['top-left' => [$start_x,$start_y],
                    'bottom-left' => [$start_x,$height -$start_y],
//                    'center' => [intval($width/2),intval($height/2)],
                    'left' => [$start_x,intval($height/2)],
                    'right' => [$width - $start_x,intval($height/2)],
                    'top-right' => [$width -$start_x, $start_y],
                    'bottom-right' => [$width - $start_x, $height - $start_y]];
//        dd($positions);
       foreach ($positions as $position => $cord)
       {

           $sended_image->text('medpravda.com.ua',$cord[0],$cord[1], function ($font) use ($position){
              $font->file(public_path().'/font/OpenSans-Bold.ttf');
              $font->size(25);
              $font->color('#e8f2f7');
//               $font->color('#7528af');
              $font->align($position);
              $font->valign($position);
              $font->angle(45);
           });
       }

        $sended_image->save(public_path().'/watermarks/'.$this->randomString(20).'.'.$image->extension());
    }

    function randomString($length = 6) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
