<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Image</h1>

    {!! Form::open([
        'route' => 'img.post',
        'method' => 'post',
        'files' => true
    ]) !!}
    {!! Form::file('image', array('class','image')) !!}
    <hr>
    <button type="submit">Send</button>
    {!! Form::close() !!}
</body>
</html>
